---
uuid: 5e09feac-2169-4d7c-9e84-90f1d2778213
hidden: false
min-servio-version: 5.0.0 # Post is hidden before this version
min-date: 2021-06-21 # Post is hidden before this date
required-permissions: # Permissions required to see the post
---
**Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.**

![Nouvelle recherche](nouvelle-recherche.png)

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

![Résulats nouvelle recherche](nouvelle-recherche-resultats.png)

